#!/bin/bash
#
# A script to run a BuildStream test case.

set -e

TEST_DIR="elements/"
RESULTS="results/"

# patch
#
# Apply patches to the test files, if necessary.
#
patch() {
	:
}

# run_test
#
# Run tests for this test case.
#
# This should create a set of directories that match the directories
# in 'results/', as well as a log of the BuildStream output in
# 'test_log.log'.
#
run_test () {
	local element
	local test_dir
	local elements
	local element_name

	patch
	mkdir -p "$TEST_DIR"
	rm -f test_log.log

	# Build tests and checkout the staged source
	elements=$(find "$TEST_DIR" -maxdepth 1 -type f)
	for element in $elements;
	do
		element_name=$(basename "$element")
		test_dir="$RESULTS/out-${element_name%.*}"

		echo "Running test '$element_name'"

		bst build "$element_name" >> test_log.log 2>&1
		bst checkout "$element_name" "$test_dir" >> test_log.log 2>&1
	done
}

run_test "$@"
