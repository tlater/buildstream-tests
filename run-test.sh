#!/bin/bash

GREEN="\e[0;32m"
YELLOW="\e[0;33m"
RED="\e[0;31m"
END="\e[0m"

main () {
	local command="$1"
	shift

	case "$command" in
		"run")
			run
			;;
		"run-test")
			run-test "$@"
			;;
		"clean")
			clean "$@"
			;;
		"omit")
			omit "$@"
			;;
		"include")
			include "$@"
			;;
		"")
			echo "Error: Please specify 'run' or 'clean'" 1>&2
			;;
		*)
			echo "Error: Unknown function '$1'" 1>&2
			;;
	esac
}

# run
#
# Run all tests in the current directory.
run () {
	local dir

	for dir in *;
	do
		if [ -d "$dir" ]
		then
			run-test "$dir"
		fi
	done
}

# clean
#
# Clean all tests in the current directory.
clean () {
	local dir

	for dir in *;
	do
		if [ -d "$dir" ]
		then
			(cd "$dir" || exit 1
			 rm -f test_log.log
			 rm -rf "results/"*)
		fi
	done
}

# run-test
#
# Run the test in the given directory
#
# Args:
#    test ($1) - The test to run
#
run-test () {
	local test="$1"

	local total=0
	local successes=0
	local test_result

	if grep -q "$test" .omit
	then
	   echo -e "${YELLOW}Skipping${END} omitted test $test."
	   return
	fi

	echo "Running tests for test case '$test'"

	(cd "$test" || exit 1
	 bash "run-$(basename "$test").sh")

	if [ ! "$?" -eq 0 ]
	then
		echo -e "Tests for '$test' ${RED}failed${END}. See '$test/test_log.log' for more information.\n" 2>&1
		return 1
	fi

	# Compare the expected and test directories
	for test_result in "$test"/results/*;
	do
		test_result="$(basename "$test_result")"
		ensure_equal "$test/results/$test_result" "$test/expected/$test_result"

		if [ "$?" -eq 0 ]
		then
			((successes++))
			printf "%-34s ${GREEN}%9s${END}\n" "$test_result" "succeeded"
		else
			printf "%-34s ${RED}%9s${END}\n" "$test_result" "failed"
		fi

		((total++))
	done

	echo -e "Tests completed. $successes out of $total tests successful.\n"
}

# omit
#
# Ignore the given test during future test runs
#
# Args:
#    test ($1) - The test to ignore
#
omit() {
	local test="$1"

	# Tell the user if we don't need to omit the file
	if grep -q "$test" .omit
	then
		echo "Test $test is already omitted." 2>&1
		exit 1
	fi

	if [ -d "$test" ]
	then
		echo "$test" >> .omit
	else
		echo "No such test." 2>&1
		exit 1
	fi
}

# include
#
# After a test has been omitted, re-include it
#
# Args:
#    test ($1) - The test to include
#
include() {
	local test="$1"

	local temp

	# Ensure we have a .omit file
	touch .omit

	# Make a temporary file to inverse grep to
	temp=$(mktemp)

	# Tell the user if we don't need to include the file
	if ! grep -q "$test" .omit
	then
		echo "Test $test is already included." 2>&1
		exit 1
	fi

	# Remove the line containing the test string
	grep -v "$test" .omit > "$temp"; mv "$temp" .omit
}

# check_permissions
#
# Compare the user execute permissions between two files.
#
# Args:
#    file_perm1 ($1) - The first file
#    file_perm2 ($2) - The second file
#
# Returns:
#    1 if the permissions mismatch
#
check_permissions () {
	local file_perm1
	local file_perm2

	# This only checks executable permissions since git will not
	# persist local permissions.
	file_perm1=$(stat -c '%A' "$1" | sed 's/...\(.\).\+/\1/')
	file_perm2=$(stat -c '%A' "$2" | sed 's/...\(.\).\+/\1/')

	if [ "$file_perm1" != "$file_perm2" ]
	then
		return 1
	fi
	return 0
}

# ensure_equal
#
# Recursively test for differences in content or permissions between
# the given directories.
#
# Args:
#    src ($1) - The first directory
#    target ($2) - The second directory
#
# Returns:
#    1 if files in the directories mismatch
#
ensure_equal () {
	local src="$1"
	local target="$2"

	local target_file
	local target_files
	local source_file

	# Check for file differences
	diff -r "$src" "$target"
	if [ $? -ne 0 ]
	then
		echo "Error: Unexpected or missing file in '$src'"
		return 1
	fi

	# Check for permission differences
	target_files=$(find "$target")
	for target_file in $target_files
	do
		source_file="$src${target_file#$target}"

		check_permissions "$source_file" "$target_file"
		if [ $? -ne 0 ]
		then
			echo "Error: File permissions differ for files '$source_file' and '$target_file'"
			return 1
		fi
	done
}

main "$@"
